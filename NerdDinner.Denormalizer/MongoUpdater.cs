﻿using System;

using MongoDB.Driver.Builders;

using NerdDinner.Messages.Events;
using NerdDinner.ReadModel;

namespace NerdDinner.Denormalizer
{
    public class MongoUpdater : BaseMongoRepository
    {

        public void Add<T>(T newObj) where T : IStoredInMongo
        {

            var db = GetDb();
            var coll = GetCollection<T>(db, newObj.CollectionName);

            try
            {
                coll.Insert(newObj);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error writing to MongoDb: {0}", e);
            }
        }

        public void Update<T>(T objToSave) where T : IStoredInMongo
        {

            var db = GetDb();
            var coll = GetCollection<T>(db, objToSave.CollectionName);

            coll.Save(objToSave);

        }

        public void Remove<T>(T objToRemove) where T : IStoredInMongo
        {

            var db = GetDb();
            var coll = GetCollection<T>(db, objToRemove.CollectionName);

            coll.FindAndRemove(Query.EQ("_id", objToRemove.Id), null);

        }

        public bool CanHandle(object body)
        {
            return (body is DinnerCreated) || (body is DinnerRescheduled);
        }
    }
}