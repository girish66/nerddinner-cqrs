﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Topshelf;

namespace NerdDinner.AppService
{
    class Program
    {
        static void Main(string[] args)
        {

            HostFactory.Run(host =>
                                {
                                    host.Service<AppService>(s =>
                                                                 {
                                                                     s.SetServiceName("NerdDinner.AppService");
                                                                     s.ConstructUsing(c => new AppService());
                                                                     s.WhenStarted(c => c.Initialize());
                                                                     s.WhenStopped(c => c.Dispose());
                                                                 });
                                    host.RunAsLocalService();
                                    host.SetDescription("NerdDinner AppService that handles WRITE operations");
                                    host.SetDisplayName("NerdDinner AppService");
                                    host.SetServiceName("NerdDinner.AppService");
                                });

        }
    }
}
