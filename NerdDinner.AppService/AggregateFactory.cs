﻿using System;
using System.Reflection;

using CommonDomain;
using CommonDomain.Persistence;

namespace NerdDinner.AppService
{
    public class AggregateFactory : IConstructAggregates
    {

        public IAggregate Build(Type type, Guid id, IMemento snapshot)
        {
            ConstructorInfo constructor = type.GetConstructor(
                                                              BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(Guid) }, null);

            return constructor.Invoke(new object[] { id }) as IAggregate;
        }

    }
}