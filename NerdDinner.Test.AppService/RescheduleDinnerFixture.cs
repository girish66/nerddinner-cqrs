﻿using System;

using Moq;

using NerdDinner.AppService.Consumers;
using NerdDinner.AppService.Domain;
using NerdDinner.Messages;
using NerdDinner.Messages.Commands;

using NUnit.Framework;

namespace NerdDinner.Test.AppService
{
    [TestFixture]
    public class RescheduleDinnerFixture : BaseDinnerConsumerFixture
    {

        [Test]
        public void RescheduleDinnerShouldLoadExistingDinnerFromRepository()
        {

            // Arrange
            var testDinnerId = Guid.NewGuid();
            var rescheduleMsg = new RescheduleDinner() { AggregateId = testDinnerId };
            _MockRepo.Setup(r => r.GetById<Dinner>(testDinnerId)).Verifiable("Did not attempt to load the dinner identified in the RescheduleDinner command");

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) { _ConsumeContext = _MockContext.Object };
            sut.Consume(rescheduleMsg);

            // Assert
            _MockRepo.VerifyAll();

        }

        [Test]
        public void WithErrorDuringLoadShouldReturnError()
        {

            // Arrange
            var testDinnerId = Guid.NewGuid();
            var rescheduleMsg = new RescheduleDinner() { AggregateId = testDinnerId };
            var testError = new Exception("UNIT TEST ERROR");
            _MockRepo.Setup(r => r.GetById<Dinner>(testDinnerId)).Throws(testError);
            _MockContext.Setup(c => c.Respond<BusResponse>(It.Is<BusResponse>(b => !b.Success && b.Exception.Equals(testError)), NullAction))
                .Verifiable("Did not response with the failure indicator and the exception thrown");

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) { _ConsumeContext = _MockContext.Object };
            sut.Consume(rescheduleMsg);

            // Assert
            _MockContext.VerifyAll();

        }

        [Test]
        public void DinnerThatDoesntExistShouldReturnError()
        {

            // Arrange
            var testDinnerId = Guid.NewGuid();
            var rescheduleMsg = new RescheduleDinner() { AggregateId = testDinnerId };
            _MockRepo.Setup(r => r.GetById<Dinner>(testDinnerId)).Returns((Dinner) null);
            _MockContext.Setup(c => c.Respond(It.Is<BusResponse>(b => !b.Success), NullAction))
                .Verifiable("Did not return a BusResponse indicating failure to load the Dinner");

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) { _ConsumeContext = _MockContext.Object };
            sut.Consume(rescheduleMsg);

            // Assert
            _MockContext.VerifyAll();

        }

        [Test]
        public void RescheduleWithErrorShouldReturnError()
        {
            
            // Arrange
            var testDinnerId = Guid.NewGuid();
            var rescheduleMsg = new RescheduleDinner() {AggregateId = testDinnerId};
            var mockDinner = new Mock<Dinner>();
            Exception testError = new Exception("UNIT TEST ERROR");
            mockDinner.Setup(d => d.RescheduleDinner(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>())).Throws(testError);
            _MockRepo.Setup(r => r.GetById<Dinner>(testDinnerId)).Returns(mockDinner.Object);

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) {_ConsumeContext = _MockContext.Object};
            sut.Consume(rescheduleMsg);

            // Assert
            _MockContext.Verify(c => c.Respond<BusResponse>(It.Is<BusResponse>(r => !r.Success && r.Exception.Equals(testError)), NullAction), "Did not return a failure message");

        }

        [Test]
        public void SaveWithoutErrorShouldReturnSuccess()
        {
            
            // Arrange
            var testDinnerId = Guid.NewGuid();
            var rescheduleMsg = new RescheduleDinner() {AggregateId = testDinnerId, NewEventTime = DateTime.Today.AddDays(1).AddHours(19)};
            var mockDinner = new Mock<Dinner>();
            _MockRepo.Setup(r => r.GetById<Dinner>(testDinnerId)).Returns(mockDinner.Object);

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) {_ConsumeContext = _MockContext.Object};
            sut.Consume(rescheduleMsg);

            // Assert
            _MockRepo.Verify(r => r.Save(mockDinner.Object, It.IsAny<Guid>(), null), "Did not call save on the Dinner");
            _MockContext.Verify(c => c.Respond(It.Is<BusResponse>(r => r.Success), NullAction), "Did not return a success indicator");

        }

        [Test]
        public void SaveWithErrorShouldReturnFailure()
        {

            // Arrange
            var testDinnerId = Guid.NewGuid();
            var rescheduleMsg = new RescheduleDinner() { AggregateId = testDinnerId, NewEventTime = DateTime.Today.AddDays(1).AddHours(19) };
            var mockDinner = new Mock<Dinner>();
            var testError = new Exception("UNIT TEST ERROR");
            _MockRepo.Setup(r => r.GetById<Dinner>(testDinnerId)).Returns(mockDinner.Object);
            _MockRepo.Setup(r => r.Save(mockDinner.Object, It.IsAny<Guid>(), null)).Throws(testError);

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) { _ConsumeContext = _MockContext.Object };
            sut.Consume(rescheduleMsg);

            // Assert
            _MockContext.Verify(c => c.Respond(It.Is<BusResponse>(r => !r.Success && r.Exception.Equals(testError)), NullAction), "Did not return a failure indicator with the appropriate exception");

        }

        [Test]
        public void RescheduleDinnerShouldSetNewEventTime()
        {
            
            // Arrange
            var newTime = DateTime.Today.AddDays(1).AddHours(19);

            // Act
            var sut = new Dinner() { EventDate = DateTime.Today.AddDays(-1)};
            sut.RescheduleDinner(newTime, DateTime.Now, "TestUser");

            // Assert
            Assert.AreEqual(newTime, sut.EventDate, "Did not change the eventDate");

        }

    }

}
