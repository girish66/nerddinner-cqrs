﻿using System;

using NerdDinner.AppService.Domain;

using NUnit.Framework;

namespace NerdDinner.Test.AppService.AggregateFactory
{
    [TestFixture]
    public class CanBuildADinner
    {

        [Test]
        public void ShouldLoadANormalDinner()
        {
            
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var sut = new NerdDinner.AppService.AggregateFactory();
            var d = sut.Build(typeof (Dinner), id, null);

            // Assert
            Assert.IsNotNull(d, "Did not create a dinner");
            Assert.IsInstanceOf<Dinner>(d, "Did not return an object of type Dinner");
            Assert.AreEqual(id, d.Id, "Did not set the id properly");

        }

    }
}