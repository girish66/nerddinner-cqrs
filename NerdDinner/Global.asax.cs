﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Security.Principal;
using System.Diagnostics;

using MassTransit;

namespace NerdDinner
{

	public class MvcApplication : System.Web.HttpApplication
	{
		public void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
					"PrettyDetails",
					"{Id}",
						new { controller = "Dinners", action = "Details" },
                        new { Id = @"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}" }
					);


			routes.MapRoute(
					"UpcomingDinners",
					"Dinners/Page/{page}",
					new { controller = "Dinners", action = "Index" }
			);

			routes.MapRoute(
					"Default",                                              // Route name
					"{controller}/{action}/{id}",                           // URL with parameters
					new { controller = "Home", action = "Index", id = "" }  // Parameter defaults
			);

            routes.MapRoute(
                "OpenIdDiscover",
                "Auth/Discover");

		}

        public override void Init()
        {
            this.AuthenticateRequest += new EventHandler(MvcApplication_AuthenticateRequest);
            this.PostAuthenticateRequest += new EventHandler(MvcApplication_PostAuthenticateRequest);
            base.Init();
        }

		void Application_Start()
		{
			RegisterRoutes(RouteTable.Routes);

		    ConfigureServiceBus();

			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new MobileCapableWebFormViewEngine());
		}

	    private void ConfigureServiceBus()
	    {

	        string mtQueue = ConfigurationManager.AppSettings["MtQueue"];

            try
            {
                Bus.Initialize(sbc =>
                                   {
                                       sbc.UseMsmq();
                                       sbc.VerifyMsmqConfiguration();
                                       sbc.ReceiveFrom(mtQueue);
                                       sbc.UseControlBus();
                                       sbc.UseSubscriptionService("msmq://localhost/NerdDinner.SubscriptionService");
                                   });
            }
            catch (Exception e)
            {
                Debug.WriteLine("-- ERROR CONFIGURING MT: {0}", e);
            }
	    }

	    void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                string encTicket = authCookie.Value;
                if (!String.IsNullOrEmpty(encTicket))
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(encTicket);
                    NerdIdentity id = new NerdIdentity(ticket);
                    GenericPrincipal prin = new GenericPrincipal(id, null);
                    HttpContext.Current.User = prin;
                }
            }
        }

        void MvcApplication_AuthenticateRequest(object sender, EventArgs e)
        {
        }
	}
}