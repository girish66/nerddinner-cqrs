﻿using System;

namespace NerdDinner.Messages.Commands
{
    public class RescheduleDinner : ICommand
    {

        public DateTime NewEventTime { get; set; }

        public Guid CorrelationId { get; set; }
        public DateTime ActingDateTimeUtc { get; set; }
        public string ActingLogonId { get; set; }
        public Guid AggregateId { get; set; }
        public int Version { get; set; }
    }
}