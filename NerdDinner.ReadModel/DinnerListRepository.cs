﻿using System;
using System.Collections.Generic;

using MongoDB.Driver.Builders;

namespace NerdDinner.ReadModel
{
    public class DinnerListRepository : BaseMongoRepository
    {
        
        public IEnumerable<DinnerListItem> FindPopularUpcomingDinners(int limit)
        {

            var db = GetDb();
            var coll = GetCollection<DinnerListItem>(db, new DinnerListItem().CollectionName);

            return coll.Find(Query.GT("dt", DateTime.Now)).SetSortOrder(SortBy.Descending("rsvp")).SetLimit(limit);

        }

    }
}