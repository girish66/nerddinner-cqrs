﻿using System;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NerdDinner.ReadModel
{
    public class DinnerListItem : IStoredInMongo
    {

        [BsonElement("t")]
        public string Title { get; set; }

        [BsonElement("dt"), BsonDateTimeOptions(DateOnly = true)]
        public DateTime EventDate { get; set; }

        [BsonElement("desc")]
        public string Description { get; set; }

        [BsonElement("rsvp")]
        public int RsvpCount { get; set; }

        [BsonElement("lat")]
        public double Latitude { get; set; }

        [BsonElement("lon")]
        public double Longitude { get; set; }

        [BsonId]
        public string Id { get; set; }

        [BsonIgnore]
        public string CollectionName { get { return "dinnerlist"; } }
    }
}