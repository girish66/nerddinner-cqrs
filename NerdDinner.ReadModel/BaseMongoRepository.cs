﻿using System;
using System.Configuration;

using MongoDB.Driver;

namespace NerdDinner.ReadModel
{
    public abstract class BaseMongoRepository
    {

        private string _ConnString;

        protected MongoDatabase GetDb()
        {

            if (string.IsNullOrEmpty(_ConnString))
            {
                _ConnString = ConfigurationManager.ConnectionStrings["nerddinner.mongo"].ConnectionString;
            }

            var svr = MongoServer.Create(_ConnString);
            return svr.GetDatabase("nerddinner");

        }

        protected MongoCollection<T> GetCollection<T>(MongoDatabase db, string collectionName)
        {

            return db.GetCollection<T>(collectionName);

        }

        public T GetById<T>(Guid id) where T: IStoredInMongo  , new()
        {
            var db = GetDb();
            var coll = GetCollection<T>(db, new T().CollectionName);

            return coll.FindOneByIdAs<T>(id.ToString());

        }

    }
}