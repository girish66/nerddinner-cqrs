﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using EventStore;

using NerdDinner.Denormalizer;
using NerdDinner.Denormalizer.Consumers;
using NerdDinner.ReadModel;

namespace NerdDinner.Rebuild
{
    class Program
    {
        private IStoreEvents _Store;

        static void Main(string[] args)
        {

            var p = new Program();

            p.Run();

        }

        private void Run()
        {

            InitializeStore();

            var handlers = IdentifyHandlers();

            var streams = _Store.Advanced.GetStreamsToSnapshot(0).ToArray();

            var eventCount = 0;

            Console.Out.WriteLine("Loading {0} streams", streams.Count());
            var sw = Stopwatch.StartNew();

            // TODO: Update this to be more generic -- so that all read-model consumers can be called
            var sau = new DinnerEventConsumer(new MongoUpdater());

            foreach (var stream in streams)
            {
                var events = _Store.OpenStream(stream.StreamId, 0, int.MaxValue).CommittedEvents;
                eventCount += events.Count;

                // Analyze and fire events against consumers here
                var sa = new DinnerListItem();
                bool handled = false;
                foreach (var e in events)
                {
                    if (sau.CanHandle(e.Body))
                    {
                        sa = sau.Consume(e.Body, sa);
                        handled = true;
                    }
                }

                if (handled) sau.Save(sa);

            }

            Console.Out.WriteLine("Updated {0} streams in {1}ms : {2:0.00}ms/stream", streams.Count(), sw.ElapsedMilliseconds, (decimal)sw.ElapsedMilliseconds / (decimal)streams.Count());
            Console.Out.WriteLine("Processed {0} events in {1}ms : {2:0.00}ms/event", eventCount, sw.ElapsedMilliseconds, (decimal)sw.ElapsedMilliseconds / (decimal)eventCount);
            Console.ReadLine();
        }

        private List<BaseConsumer> IdentifyHandlers()
        {
            throw new NotImplementedException();
        }

        private void InitializeStore()
        {
            if (_Store == null)
            {

                _Store = Wireup.Init()
                .UsingSqlPersistence("NerdDinnerSQL")
                .InitializeStorageEngine()
                .UsingJsonSerialization()
                    //.UsingBinarySerialization()
                .Build();

            }
        }
    }
}
